<?php

namespace backend\modules\cars\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "brand_cars".
 *
 * @property int $id
 * @property string $name
 */
class BrandCars extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brand_cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    public static function getList()
    {
        $list = self::find()->asArray()->all(); 
        return ArrayHelper::map($list, 'id', 'name'); 
    }

}
