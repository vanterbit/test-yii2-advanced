<?php

namespace backend\modules\cars\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "color_cars".
 *
 * @property int $id
 * @property string $color
 */
class ColorCars extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'color_cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['color'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'color' => 'Color',
        ];
    }

    public static function getList()
    {
        $list = self::find()->asArray()->all();
        return ArrayHelper::map($list, 'id', 'color');
    }

}
