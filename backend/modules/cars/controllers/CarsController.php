<?php

namespace backend\modules\cars\controllers;

use Yii;
use backend\models\Cars;
use backend\models\User;
//use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\modules\cars\models\BrandCars;
use backend\modules\cars\models\ColorCars;

/**
 * CarsController implements the CRUD actions for Cars model.
 */
class CarsController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'view'],
                        'roles' => ['admin'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'delete', 'update'],
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all Cars models.
     * @return mixed
     */
    public function actionIndex()
    {
        $colors = ColorCars::getlist();
        $brands = BrandCars::getlist();
        $carList = Cars::find()->all();
        $usersList = User::getlist();
        return $this->render('index', [
                    'carList' => $carList,
                    'colors' => $colors,
                    'brands' => $brands,
                    'usersList' => $usersList,
        ]);
    }

    /**
     * Displays a single Cars model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Cars model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cars();
        $colors = ColorCars::getlist();
        $brands = BrandCars::getlist();
        $usersList = User::getlist();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Автомобиль добавлен!');
//            return $this->redirect(['view', 'id' => $model->id]);
            return $this->redirect('index');
        }

        return $this->render('create', [
                    'model' => $model,
                    'colors' => $colors,
                    'brands' => $brands,
                    'usersList' => $usersList,
        ]);
    }

    /**
     * Updates an existing Cars model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $colors = ColorCars::getlist();
        $brands = BrandCars::getlist();
        $usersList = User::getlist();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Данные обновлены!');
            return $this->redirect('index');
        }
        return $this->render('update', [
                    'model' => $model,
                    'colors' => $colors,
                    'brands' => $brands,
                    'usersList' => $usersList,
        ]);
    }

    /**
     * Deletes an existing Cars model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('info', 'Автомобиль удален!');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Cars model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cars the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cars::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
