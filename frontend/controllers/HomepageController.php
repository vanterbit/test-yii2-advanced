<?php

namespace frontend\controllers;

use Yii;
use yii\web\Controller;

/**
 * Домашняя страница пользователь
 *
 * @author Prot
 */
class HomepageController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex() {
        return $this->render('index');
    }

}
