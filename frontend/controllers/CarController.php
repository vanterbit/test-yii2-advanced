<?php

namespace frontend\controllers;

use Yii;
use frontend\models\Car;
//use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use frontend\models\ColorCars;
use frontend\models\BrandCars;
use frontend\controllers\behaviors\AccessBehavior;

/**
 * CarController implements the CRUD actions for Car model.
 */
class CarController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            AccessBehavior::className(),
        ];
    }

    /**
     * Lists all Car models.
     * @return mixed
     */
    public function actionIndex() {
        $colors = ColorCars::getlist();
        $brands = BrandCars::getlist();
//        $carList = Car::find()->all();
        $carList = Car::find()
                ->where(['id_user' => Yii::$app->user->id])
                ->all();
        return $this->render('index', [
                    'carList' => $carList,
                    'colors' => $colors,
                    'brands' => $brands,
        ]);
    }

    /**
     * Displays a single Car model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $colors = ColorCars::getlist();
        $brands = BrandCars::getlist();

        return $this->render('view', [
                    'model' => $this->findModel($id),
                    'colors' => $colors,
                    'brands' => $brands,
        ]);
    }

    /**
     * Creates a new Car model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Car();
        $colors = ColorCars::getlist();
        $brands = BrandCars::getlist();
        
        $model->id_user = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->number_car]);
            Yii::$app->session->setFlash('success', 'Автомобиль добавлен!');
            return $this->redirect(['car/index']);
        }

        return $this->render('create', [
                    'model' => $model,
                    'colors' => $colors,
                    'brands' => $brands,
        ]);
    }

    /**
     * Updates an existing Car model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        $colors = ColorCars::getlist();
        $brands = BrandCars::getlist();
        $model->id_user = Yii::$app->user->id;

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Автомобиль обновлен!');
            return $this->redirect('index');
        }

        return $this->render('update', [
                    'model' => $model,
                    'colors' => $colors,
                    'brands' => $brands,
        ]);
    }

    /**
     * Deletes an existing Car model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = Car::findOne($id);
        $model->delete();
        Yii::$app->session->setFlash('success', 'Автомобиль удален!');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Car model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Car the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Car::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    
    public function actionGuest()
    {
        return $this->render('/car/guest');
    }

}
