<?php
/* @var $this yii\web\View */

use yii\helpers\Url;

$this->title = 'Cars SERVICE';
?>
<div class="site-index">

    <div class="jumbotron">
        <h2>Welcome to Cars SERVICE</h2>


        <p><a class="btn btn-lg btn-success" href="<?php  echo Yii::$app->urlManager->createUrl("cars"); ?>">Добавить автомобиль</a></p>
        <p><a class="btn btn-default" href="http://adminka.web.myweb.in.ua/admin">Админка</a></p>
    </div>

    <div class="body-content">

    </div>
</div>
