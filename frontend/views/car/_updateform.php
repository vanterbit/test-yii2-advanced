<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model frontend\models\Car */
/* @var $form yii\widgets\ActiveForm */
/* @var $colors array */
/* @var $brands array */
?>

<div class="car-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id_color')->dropDownList($colors) ?>

    <?= $form->field($model, 'id_brand')->dropDownList($brands) ?>

    <?= $form->field($model, 'number_car')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
