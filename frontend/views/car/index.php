<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cars';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="car-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Car', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <table class="table table-condensed">
        <tr>
            <th>Цвет</th>
            <th>Марка</th>
            <th>Гос. номер</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
        <?php foreach ($carList as $car): ?>
        <tr>
            <td> <?php echo $colors[$car->id_color]; ?></td>
            <td> <?php echo $brands[$car->id_brand]; ?></td>
            <td> <?php echo $car->number_car; ?></td>
            <td><a href="<?php echo Url::to(['car/update', 'id' => $car->id]); ?>"><span class="glyphicon glyphicon-pencil"></span></a></td>
            <td><a href="<?php echo Url::to(['car/delete', 'id' => $car->id]); ?>" data-confirm="Вы уверены, что хотите удалить автомобиль?" data-method="post"><span class="glyphicon glyphicon-trash"></span></a></td>
            <td></td>

        </tr>

        <?php endforeach; ?>
    </table>
</div>
