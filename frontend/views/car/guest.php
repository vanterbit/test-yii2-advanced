<?php

use yii\helpers\Html;
use yii\helpers\Url;

?>
<h2>Сервис доступен только для зарегестрированых пользователей</h2>
<br><br>

 <?= Html::a('Зарегистрируйтесль', ['/site/signup'], ['class' => 'btn btn-primary center-block']) ?>
<br>  <br>
 <?= Html::a('Залогинтесь', ['/site/login'], ['class' => 'btn btn-success center-block']) ?>
