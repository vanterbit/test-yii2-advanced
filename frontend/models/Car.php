<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "user_cars".
 *
 * @property int $id
 * @property int $id_user
 * @property int $id_color
 * @property int $id_brand
 * @property string $number_car
 */
class Car extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'user_cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['id_user', 'number_car'], 'required'],
            [['id_user', 'id_color', 'id_brand'], 'integer'],
            [['number_car'], 'string', 'max' => 255],
            [['number_car'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'id_user' => 'Id User',
            'id_color' => 'Цвет',
            'id_brand' => 'Марка',
            'number_car' => 'Гос. номер',
        ];
    }
    
    /**
     * Получаем данные о автомобилях юзера по id user
     * 
     * @param type $idUser
     * @return array
     */
    public static function getCarData($idUser) {
        $CarData = Car::find()
                ->where(['id_user' => $idUser])
                ->one();

        return $CarData;
    }

}
