<?php

namespace frontend\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "color_cars".
 *
 * @property int $id
 * @property string $color
 */
class ColorCars extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'color_cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['color'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => 'ID',
            'color' => 'Color',
        ];
    }

    public static function getList() {
        $list = self::find()->asArray()->all(); //asArray - указываем что нужно вернуть массивы
        return ArrayHelper::map($list, 'id', 'color'); //групируем масив
    }

}
