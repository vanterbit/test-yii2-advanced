<?php

namespace frontend\models;

use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "brand_cars".
 *
 * @property int $id
 * @property string $name
 */
class BrandCars extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brand_cars';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Марка',
        ];
    }
    
    public static function getList() {
        $list = self::find()->asArray()->all(); //asArray - указываем что нужно вернуть массивы
        return ArrayHelper::map($list, 'id', 'name'); //групируем масив
    }

}
