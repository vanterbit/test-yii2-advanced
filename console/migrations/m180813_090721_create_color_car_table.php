<?php

use yii\db\Migration;

/**
 * Handles the creation of table `color_car`.
 */
class m180813_090721_create_color_car_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up() {
        $this->createColor();
    }

    /**
     * {@inheritdoc}
     */
    public function down() {
        $this->dropTable('color_cars');
    }

    
    private function createColor() {
        $this->createTable('color_cars', [
            'id' => $this->primaryKey(),
            'color' => $this->string(),
        ]);

        $colors = $this->colors(); 
        foreach ($colors as $id => $color) {
            $this->insert('color_cars', [
                'id' => $id,
                'color' => $color,
            ]);
        }
    }
    
     private function colors() {

        return $colors = [
            1 => 'Red',
            2 => 'Black',
            3 => 'Green',
            4 => 'Blue',
            5 => 'White',
            ];
     }

}
