<?php

use yii\db\Migration;

/**
 * Handles the creation of table `brand_cars`.
 */
class m180813_092750_create_brand_cars_table extends Migration 
{

    /**
     * {@inheritdoc}
     */
    public function up() {
        $this->createBrand();
    }

    /**
     * {@inheritdoc}
     */
    public function down() {
        $this->dropTable('brand_cars');
    }

    
    private function createBrand() {
        $this->createTable('brand_cars', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
        ]);

        $brand = $this->brands(); 
        foreach ($brand as $id => $name) {
            $this->insert('brand_cars', [
                'id' => $id,
                'name' => $name,
            ]);
        }
    }
    
     private function brands() {

        return $brands = [
            1 => 'Mazda',
            2 => 'Mercedes',
            3 => 'BMW',
            4 => 'Opel',
            5 => 'Honda',
            ];
     }

}
