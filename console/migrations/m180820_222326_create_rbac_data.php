<?php

use yii\db\Migration;
//use backend\models\User;
use common\models\User;

/**
 * Class m180820_222326_create_rbac_data
 */
class m180820_222326_create_rbac_data extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $auth = Yii::$app->authManager;

        // Define permissions
        $managerPermission = $auth->createPermission('editCars');
        $auth->add($managerPermission);

        // Define roles
        $moderatorRole = $auth->createRole('moderator');
        $auth->add($moderatorRole);

        $adminRole = $auth->createRole('admin');
        $auth->add($adminRole);

        // Define roles - permissions relations
        $auth->addChild($moderatorRole, $managerPermission);
        $auth->addChild($adminRole, $moderatorRole);

        // Create admin user
        $user = new User([
            'email' => 'admin@admin.com',
            'username' => 'Admin',
            'password_hash' => '$2y$13$0usUeSsdBOq306LpMOrqkuVBxj3AMq/hWJPhQgReTFC5buI9IPuwa',//123456
        ]);
        $user->generateAuthKey();
        $user->save();
        
        // Add admin role to user 
        $auth->assign($adminRole, $user->getId());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180820_222326_create_rbac_data cannot be reverted.\n";

        return false;
    }

}
