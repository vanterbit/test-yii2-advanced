<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_cars`.
 */
class m180813_092601_create_user_cars_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('user_cars', [
            'id' => $this->primaryKey(),
            'id_user' => $this->integer()->notNull(),
            'id_color' => $this->integer(),
            'id_brand' => $this->integer(),
            'number_car' => $this->string()->notNull()->unique(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('user_cars');
    }
}
